\section{Interpolación polinomica}
\subsection{Motivación}
\begin{frame}
\frametitle{Motivación}
El problema de interpolación polinómica consiste en, dados $n+1$
pares distintos ${(z_{j}, w_{j}) \in \CC \times \CC, \quad j \in I_{n}}$\footnote{$I_n=\{0,1,\ldots, n\}$.}, encontrar un polinomio $\Pi(z)$ tal que $\Pi(z_{j}) = w_{j}$.
\end{frame}

\begin{frame}
Las preguntas siguientes surgen de manera natural:

\begin{enumerate}
\item<1-> Dada $f\in C([a,b])$, ?`existe el polinomio de interpolación, con grado prefijado, para aproximar a $f$? ({\em Existencia})

\item<2-> Si existe ese polinomio, ?`es este \'{u}nico? ({\em Unicidad})

\item<3-> ?`C\'{o}mo puede determinarse, si existe,
    el polinomio que interpola a $f$? ({\em Construcci\'{o}n})
\end{enumerate}

\end{frame}

\begin{frame}
\begin{theorem}\label{interp}
Dados $n+1$ pares de interpolación distintos ${(z_{j}, w_{j}), \quad j \in I_{n}}$, existe un polinomio \'{u}nico $\Pi_n(z) \in \PP_{n}$ tal que ${\Pi_n(z_{j}) = w_{j}, \quad j \in I_{n}}$.
\end{theorem}

\begin{proof}
Idea: Solo basta tomar un polinomio $P\in \PP_n$ que satisfaga las condiciones dadas y estudiar la existencia y únicidad de la solución del sistema de ecuaciones resultante.
\end{proof}
\end{frame}

\begin{frame}
Cuando los $n+1$ pares de interpolación provengan del grafo de una función conocida $f$, estaremos representando al polinomio que pasa por tales puntos por $\Pi_nf(z)$.
\end{frame}

\begin{frame}
Al momento de hacer la demostración del teorema (\ref{interp}) nos encontramos con el sistema de ecuaciones siguiente

\begin{equation}\label{system_interp}
\left(\begin{array}{ccccc}
1 & z_0 & z_0^2 & \cdots & z_0^n\\
1 & z_1 & z_1^2 & \cdots & z_1^n\\
\vdots & \vdots & \vdots &  & \vdots \\
1 & z_n & z_n^2 & \cdots & z_n^n \end{array}\right )
\left(\begin{array}{c} c_0\\c_1\\ \vdots \\c_n \end{array}\right)=
\left(\begin{array}{c} w_0\\w_1\\ \vdots \\w_n \end{array}\right)
\end{equation}

El cual resulta tedioso de resolver, y nos limita al momento de construir el polinomio de interpolación. Afortunadamente, el teorema mantiene su validez si se construye el polinomio en una base distinta a la usual. Centraremos nuestra atención en esto último.
\end{frame}

\subsection{Forma de Lagrange}
\begin{frame}
\frametitle{Forma de Lagrange}

Consideremos $n+1$ pares de interpolación distintos $\{(z_j, w_j)\}_{j=0}^n$. Construiremos unos polinomios que servirán como base para expresar el polinomio de interpolación $\Pi_n(z)$. Veamos.\\

Sea $\omega_{n+1}(z)$ el polinomio monico dado por

\begin{equation}\label{polimonico}
\omega_{n+1}(z)=(z-z_0)(z-z_1) \ldots (z-z_n)=\prod_{j=0}^n(z-z_j).
\end{equation}

Es claro que para $z_j, \quad j \in I_n$, $\omega_{n+1}(z_j) = 0$. Sea ahora $\omega_{n,k}(z)$, el cual verifica $\omega_{n,k}(z_k) \not = 0$, dado por

\begin{equation}\label{lagrange1}
\omega_{n,k}(z)=\prod_{{j=0}\atop{j\neq k}}^n(z-z_j)
\end{equation}
\end{frame}

\begin{frame}
Por ultimo, sea $L_{n,k}(z) = \frac{\omega_{n+1}(z)}{\omega_{n,k}(z)}$. Es claro que $L_{n,k}(z_{k})=1$ y $L_{n,k}(z_j)=0$, para $j \not = k$. Luego,

\begin{equation}\label{coeflagrange}
L_{n,k}(z)=\prod_{{j=0}\atop{j\neq k}}^n\frac {z-z_j}{z_k-z_j}.
\end{equation}

y asi, hemos construido el conjunto de polinomios siguiente:

$${L = \{L_{n,k}(z) \in \PP_n \, | \, k \in I_n\}}.$$

?`Es $L$ una base de $\PP_n$?
\end{frame}

\begin{frame}
\begin{lemma}\label{lagrangeformlemma}
El conjunto $L = \{L_{n,k}(z) \in \PP_n \, | \, k \in I_n\} \subset \PP_n$ es una base del espacio vectorial $\PP_n$.
\end{lemma}

\begin{proof}
Idea: Por cada conjunto de $n+1$ pares de interpolación distintos $\{(z_j,w_j)\}_{j=0}^n$, existe un polinomio $P\in \PP_n$ único tal que $P(z_j)=w_j$. Luego, expresamos $\Pi_n$ como combinación lineal de los $L_{n,k}$ y permitimos que $\Pi_n(z_j)=w_j$. Si definimos $Q(z) = \Pi_n(z) - P(z)$, podemos ver que $Q$ se anula en $n+1$ puntos, pero $Q \in \PP_n$, con lo que $Q(z) = 0$. Asi que $\Pi_{n}(z) = P(z)$. Luego tenemos un sistema de ecuaciones que posee solución única y concluimos que $L$ es una base de $\PP_n$.
\end{proof}
\end{frame}

\begin{frame}
El sistema de ecuaciones que resulta es el siguiente

\begin{equation}\label{base}
\left(\begin{array}{ccccc}
1 & 0 & 0 & \cdots & 0\\
0 & 1 & 0 & \cdots & 0\\
\vdots & \vdots & \vdots &  & \vdots \\
0 & 0 & 0 & \cdots & 1
\end{array}\right )
\left(\begin{array}{c} c_0\\c_1\\ \vdots \\c_n \end{array}\right)=
\left(\begin{array}{c} w_0\\w_1\\ \vdots \\w_n \end{array}\right)
\end{equation}

Asi que la solución de (\ref{base}) esta dada por
$$c_j = w_j, \quad j \in I_n.$$
\end{frame}

\begin{frame}
\begin{definition}
Los polinomios $L_{n,k}(z)$ de la base $L$ se conocen como {\em polinomios fundamentales}.
\end{definition}
\end{frame}


\begin{frame}
Ahora estamos en condiciones de expresar el polinomio de interpolación.

\begin{definition}[Forma de Lagrange]
Consideremos $n+1$ pares de interpolación distintos $\{(z_j,w_j)\}_{j=0}^n$. El polinomio $\Pi_n(z)$ dado por

$$\Pi_n(z)=\sum_{k=0}^nw_kL_{n,k}(z),$$

se conoce como {\em forma de Lagrange} del polinomio de interpolación.
\end{definition}

Inmediatamente, podemos apreciar que $\Pi_n(z_k) = w_k$. Tal como deseabamos.
\end{frame}

\begin{frame}
\begin{observation}
Hay una desventaja al utilizar los polinomios $L_{n,k}(z)$, ya que si deseamos agregar un nuevo nodo de interpolación, digamos $z_{n+1}$, entonces tenemos que recalcular cada uno de los polinomios fundamentales $L_{n+1,k}(z)$. En lo sucesivo presentaremos una base alternativa que nos permitirá añadir nodos de interpolación con un menor costo computacional.
\end{observation}
\end{frame}

\subsection{Nodos equiespaciados}
\begin{frame}
\frametitle{Nodos de interpolación uniformemente espaciados}

Los nodos $z_i, \quad i\in I_n$ puedes ser tomados arbitrariamente, pero centraremos nuestra a atención a cuando estos están {\em uniformemente espaciados}. Como ya veremos, los polinomios (\ref{coeflagrange}) se simplifican considerablemente cuando se construyen sobre estos nodos.\\

Escojamos nodos de interpolación $z_j, \quad j\in I_n$, tales que ${|z_j-z_{j-1}| = h}$, donde $h$ es una constante real no negativa, ${j = 1,2, \ldots, n}$. Asi que tenemos que cada $z_j$ se encuentra en la circunferencia de radio $h$ y centro $z_{j-1}$, tenemos infinito de ellos. Simplifiquemos la selección tomando los $z_{j}$ sobre un rayo $\mathcal{L}$ fijo. Así que, para algún $\theta \in \RR$, tenemos

$$z_j = z_{0} + jhe^{i\theta}, \quad j\in I_n$$
\end{frame}

\begin{frame}
As\'{\i}, la ecuaci\'{o}n (\ref{coeflagrange}) se transforma en:

\begin{equation}\label{punto_equi2}
L_{n,k}(z)=\prod_{{j=0}\atop{j\neq k}}^n\frac{z-z_0-jhe^{i\theta}}{(k-j)he^{i\theta}}=\prod_{{j=0}\atop{j\neq k}}^n\frac{t-j}{k-j}
\end{equation}

donde $$t=\frac{z-z_0}{he^{i\theta}}.$$
\end{frame}

\begin{frame}
Como

\begin{eqnarray}\label{punto_equi3}
\prod_{{j=0}\atop{j\neq
k}}^n\frac{1}{k-j}&=&\left(\prod_{j=0}^{k-1}\frac{1}{k-j}\right)\left(\prod_{j=k+1}^n\frac{1}{k-j}\right) \nonumber\\
&=&\frac{1}{k!}\frac{(-1)^{n-k}}{(n-k)!}=\frac{(-1)^{n-k}}{n!}\left(\begin{array}{c}n\\k\end{array}\right)
\end{eqnarray}

Se tiene que la expresi\'{o}n para $L_{n,k}(z)$ dada en (\ref{punto_equi2}) quedar\'{\i}a expresada de la siguiente manera

\begin{equation}
L_{n,k}(z)=\frac{(-1)^{n-k}}{n!}
\left(\begin{array}{c}n\\k\end{array}\right)\prod_{{j=0}\atop{j\neq k}}^n(t-j)
\end{equation}
\end{frame}

\subsection{Forma de Newton}
\begin{frame}
\frametitle{Forma de Newton}

Al momento de presentar la forma de Lagrange del polinomio de interpolación, presentamos un inconveniente al momento de agregar un nuevo nodo al conjunto de nodos de interpolación: Tendriamos que recalcular cada polinomio $L_k$, lo cual no es muy eficiente. ?`Existe alguna una manera recursiva para construir el polinomio de interpolación? Eso lo veremos en los sucesivo.
\end{frame}

\begin{frame}
Consideremos los siguientes polinomios recursivos

\begin{equation}\label{newtonrec}
N_j(z) = (z-z_{j-1})N_{j-1}(z), \quad N_0 = 1.
\end{equation}

Alguno de estos son: $N_{0}(z) = 1$, $N_1(z) = (z-z_0)$, $N_2(z) = (z-z_0)(z-z_1)$, \ldots, $N_j(z) = (z-z_0)(z-z_1)\dots(z-z_{j-1})$. Tenemos, al remover la recursividad,

$$N_j(z) = \prod_{k=0}^{j-1} z-z_k.$$

De modo que definimos el conjunto $N=\{N_j(z) \, | \, j \in I_n\}$. ?`Es $N$ una base de $\PP_n$?
\end{frame}

\begin{frame}
\begin{lemma}\label{newtonformlemma}
Si $z_j, \quad j \in I_n$ son $n+1$ puntos y $N=\{N_j(z) \, | \, j \in I_n\}$, entonces $N$ es una base del espacio $\PP_n$.
\end{lemma}

\begin{proof}
Idea: Se basa en la misma idea de la demostración del lema (\ref{lagrangeformlemma}).
\end{proof}
\end{frame}

\begin{frame}
La matriz de coeficientes del sistema de ecuaciones que resulta es el siguiente

\begin{equation}\label{matrix_newton}
\left(\begin{array}{ccccc}
\dsty{\prod_{k=0}^{-1} (z_0-z_k)} & 0 & \cdots & 0\\
\dsty{\prod_{k=0}^{-1} (z_1-z_k)} & \dsty{\prod_{k=0}^{0} (z_1-z_k)}  & \cdots & 0\\
\vdots & \vdots & \vdots & \vdots &  \\
\dsty{\prod_{k=0}^{-1} (z_n-z_k)} & \dsty{\prod_{k=0}^{0} (z_n-z_k)} & \cdots & \dsty{\prod_{k=0}^{n-1} (z_n-z_k)}
\end{array}\right )
\end{equation}
\end{frame}

\begin{frame}
Sean $\{(z_j, w_j)\}_{j=0}^n$ $n+1$ pares de interpolación, por el teorema (\ref{interp}), existe un único $\Pi_n \in \PP_n$ tal que $\Pi_n(z_j) = w_j, \quad j\in I_n$. Si escribimos $\Pi_n$ en la base $N$, entonces existen únicos escalares $c_j, \quad j\in I_n$ tales que

\begin{equation}\label{polinewton}
\Pi_n(z)=\sum_{j=0}^nc_j\prod_{k=0}^{j-1} (z-z_k).
\end{equation}

Al polinomio (\ref{polinewton}) se le conoce como {\em forma de Newton} del polinomio de interpolación.
\end{frame}

\begin{frame}
\begin{observation}\label{obsnewton}
\begin{enumerate}
\item[1] En la forma de Newton del polinomio de interpolación no se ha exigido que los nodos de interpolación sean distintos. Mas adelante estudiaremos la situación cuando los nodos son coincidentes.
\end{enumerate}
\end{observation}
\end{frame}

\addtocounter{observation}{-1}
\begin{frame}
\begin{observation}[cont.]
\begin{enumerate}
\item[2] Es claro que no se han determinado los coeficientes $c_j, \quad j\in I_n$. Note que el teorema (\ref{interp}) garantiza la unicidad de $\Pi_n(z)$. Luego,

$$\Pi_n(z) = \sum_{k=0}^n w_kL_{n,k}(z) = \Pi_{n-1} + c_n\prod_{k=0}^{n-1} (z-z_k).$$

Por tanto, $c_n$ es el coeficiente del termino $z^n$ en la forma de Lagrange del polinomio que interpola $\{(z_j,w_j)\}_{j=0}^n$. Tomando en cuenta la misma idea, como veremos, podemos encontrar los $c_j$ restantes.
\end{enumerate}
\end{observation}
\end{frame}

\begin{frame}
\begin{lemma}\label{coefnewton}
Si $\{(z_j, w_j)\}_{j=0}^n$ son $n+1$ pares de interpolación distintos, entonces los coeficientes $c_j, \quad j\in I_n$, de la forma de Newton para el polinomio de interpolaci\'{o}n vienen dados por

\begin{equation}
c_m=\sum_{k=0}^m\frac{w_k}{\omega_{m,k}(z_k)}, $$donde$$
\omega_{m,k}(z_k)=\prod_{j=0\atop j\neq k}^m(z_k-z_j)
\end{equation}
\end{lemma}
\end{frame}

\begin{frame}
\begin{proof}
Idea: Consideremos, para cada $m\in I_n$, los siguientes pares de interpolación
$\{z_j,w_j\}_{j=0}^m \subset \{(z_j,w_j)\}_{j=0}^n$. Luego proceda con la idea de la observación (\ref{obsnewton}) sobre $\{z_j,w_j\}_{j=0}^m$.
\end{proof}
\end{frame}



\begin{frame}
Considerando el caso de nodos equiespaciados sobre algun rayo $\mathcal{L}$ fijo, ${z_j=z_0+jhe^{i\theta}, \quad j \in I_n}$, podemos utilizar la ecuación (\ref{punto_equi3}) para obtener

\begin{equation}
\frac{1}{\omega_{n,k}(z_k)}=\prod_{{j=0}\atop{j\neq
k}}^n\frac{1}{(z_k-z_j)} =\frac{(-1)^{n-k}}{n!(he^{i\theta})^n}\binom{n}{k}
\end{equation}

y así

\begin{equation}\label{newtonesp}
c_n=\frac{1}{n!(he^{i\theta})^n}\sum_{k=0}^n(-1)^{n-k}\binom{n}{k}w_k
\end{equation}
\end{frame}

\begin{frame}
\begin{observation}\label{obsdifdiv}
Dado que ${\dsty \Pi_n(z) = \sum_{j=0}^nc_j\prod_{k=0}^{j-1} (z-z_k) = \sum_{j=0}^{n-1}c_j\prod_{k=0}^{j-1} (z-z_k) + c_n\prod_{k=0}^{n-1}(z-z_k)}$, se tiene

$$\Pi_n(z) = \Pi_{n-1}(z) + c_n\omega_{n}(z),$$

donde $\omega_n(z)$ es el polinomio mónico de grado $n$ que se anula en los puntos $z_j, \quad j \in I_{n-1}$. Asi que $\omega_{n}(z_n) \not = 0$. En efecto,

$$c_n = \frac{\Pi_n(z_n) - \Pi_{n-1}(z_n)}{\omega_n(z_n)} = \frac{w_n - \Pi_{n-1}(z_n)}{\omega_n(z_n)}.$$
\end{observation}
\end{frame}

\subsection{Diferencias divididas}
\begin{frame}
\frametitle{Diferencias divididas}
Dado un conjunto de pares de puntos $\{(z_j,w_j)\}_{j=0}^n$, consideremos las siguientes relaciones recurrentes

$$[w_0] = w_0,$$
\begin{equation}\label{difdiv}
[w_0, \ldots, w_{n-1}, w_n]=\frac{[w_1, \ldots, w_{n-1}, w_n]-[w_0, \ldots, w_{n-2}, w_{n-1}]}{z_n-z_0}.
\end{equation}
\end{frame}

\begin{frame}
Los primeros tres coeficientes de la forma de Newton:
\begin{itemize}
\item $c_0 = w_0 = [w_0]$.

\item $\dsty c_1 = \frac{w_1 - w_0}{z_1 - z_0} = [w_0, w_1].$

\item $\dsty c_2 = \frac{\frac{w_2 - w_0}{z_2-z_1} - \frac{w_1 - w_0}{z_1 - z_0}\frac{z_2-z_0}{z_2-z_1}}{z_2-z_0} = \frac{\frac{w_2 - w_1}{z_2-z_1} - \frac{w_1 -w_0}{z_1-z_0}}{z_2 - z_0} = [w_k]_{k=0}^2$.
\end{itemize}

?`$c_n = [w_k]_{k=0}^n$?
\end{frame}

\begin{frame}
  \begin{lemma}\label{coefnewtondiff}
  Sean $\{(z_j, w_j)\}_{j=0}^n \subset \CC \times \CC$ pares de interpolación. Entonces

  $$[w_0, \ldots, w_n] = \sum_{k=0}^n\frac{w_k}{\dsty \prod_{j=0\atop j\neq k}^n(z_k-z_j)}.$$
  \end{lemma}

  \begin{proof}
    Idea: Inducción.
  \end{proof}
\end{frame}

\begin{frame}
Supogamos que transponemos $w_{n-1}$ y $w_0$. ?`Se mantienen invariantes las diferencias divididas bajo transposición de indices?

\begin{definition}
Sea $X$ un conjunto no vacio y $\phi : X \longrightarrow X$. Se dice que $\phi$ es una {\em permutación} sobre $X$ si $\phi$ es biyectiva.
\end{definition}
\end{frame}

\begin{frame}
  \begin{lemma}
  Sean $\{(z_j, w_j)\}_{j=0}^n \subset \CC \times \CC$ pares de interpolación. Si $\phi$ es una permutación sobre $I_n$, entonces

  $$[w_{\phi(0)}, \ldots, w_{\phi(n-1)},w_{\phi(n)}] = [w_0, \ldots, w_{n-1},w_n].$$
  \end{lemma}

  \begin{proof}
  Idea: Se sigue inmediatamente por el lema (\ref{coefnewtondiff}).
  \end{proof}
\end{frame}

\begin{frame}
  Las diferencias divididas $[w_0, \ldots, w_{n-1}, w_n]$ serán denotadas, en lo sucesivo, por $[w_j]_{j=0}^n$.\\[0.5cm]

Por el lema (\ref{coefnewtondiff}), tenemos

\begin{equation}
\Pi_n(z)=\sum_{j=0}^n[w_m]_{m=0}^j\prod_{k=0}^{j-1} (z-z_k)
\end{equation}

Esta última se conoce como forma de Newton del polinomio de interpolación por {\em diferencias divididas}.
\end{frame}

\begin{frame}
  \begin{table}[h]
  \scalebox{0.6}{
  \begin{tabular}{ccccc}
  \hline  ${z_i}$  &   $[w_i]$  &   $[w_i,w_{i+1}]$ &    $[w_i,w_{i+1},w_{i+2}]$ & $\cdots$
  \\ \hline \\ [0.5ex]
  $z_0$ & $ [w_0]$ & & &\\
  &  &  $ [w_0,w_1]=\frac{\dsty w[z_1]-w[z_0] }{\dsty z_1-z_0}$ &  & \\
  $z_1$ & $[w_1]$ & & $[w_0,w_1,w_2]=\frac{\dsty [w_1,w_2]-[w_0,w_1] }{\dsty z_2-z_0}$ & \\
  & & $[w_1,w_2] = \frac{\dsty [w_2]-[w_1] }{\dsty z_2-z_1}$ &  & \\
  $z_2$ & $[w_2]$ & &
  $[w_1,w_2,w_3] = \frac{\dsty [w_2,w_3]-[w_1,w_2] }{\dsty z_3-z_1}$ & \\
  & & $[w_2,w_3] = \frac{\dsty [w_3]-[w_2] }{\dsty z_3-z_2}$ &  & \\
  $z_3$ & $[w_3]$ & & $\vdots$ & \\
  & & $\vdots$ &  &\\
  $\vdots$ & $\vdots$ & & $[w_{n-2},w_{n-1},w_n] = \frac{\dsty [w_{n-1},w_n]-[w_{n-2},w_{n-1}] }{\dsty z_n-z_{n-2}}$ & \\
  & & $[w_{n-1},w_n] = \frac{\dsty [w_n]-[w_{n-1}] }{\dsty z_n-z_{n-1}}$ &  &\\
  $z_n$ & $[w_n]$ & & &\\[1,5ex] \hline
  \end{tabular}}
  \caption{Esquema para las diferencias divididas}
  \end{table}
\end{frame}

\subsection{Forma de Hermite}
\begin{frame}
\frametitle{Forma de Hermite}
Hemos presentado la teoria de interpolación sobre nodos distintos. Nuestro objetivo inicial encontrar un polinomio que coincidiera con los valores de una
funci\'on en unos puntos dados. Iremos un poco mas allá, consideramos nodos coincidentes.
\end{frame}

\begin{frame}
  Empecemos por conseguir un polinomio \'{u}nico $\Pi$ tal que

  \begin{equation}\label{condhermite}
  \Pi(z_j)=w_j\qquad \Pi'(z_j)=w'_j,\qquad i\in I_n
  \end{equation}

  donde $\{(z_j, w_j), (z_j, w'_j)\}_{j=0}^n$ es un conjunto de $2n+2$ pares. Note que en (\ref{condhermite}) hay $2n+2$ condiciones, por lo tanto necesitamos un polinomio $\Pi_{2n+1}(z) \in \PP_{2n+1}$.
\end{frame}

\begin{frame}
Se construirá una base adecuada, como en la forma de Lagrange.\\

Sean $\alpha_j(z), \beta_j(z) \in \PP_{2n+1}$ tales que

\begin{equation}\label{polyhermite}
\Pi_{2n+1}(z)=\sum_{j=0}^n[a_j\alpha_j(z)+b_j\beta_j(z)].
\end{equation}

\begin{observation}\label{obs_condHermite}
Es claro que $\Pi_{2n+1}(z)$ satisface (\ref{condhermite}), si:

\begin{enumerate}
\item $\alpha_j(z_k) = \beta'_j(z_k) = \delta_{jk}$.
\item $\alpha'_j(z_k) = \beta_j(z_k) = 0$.
\end{enumerate}
para $i,j\in I_n$
\end{observation}
\end{frame}

\begin{frame}
Consideremos la base de Lagrange $L_n$, es claro que $L_{n,j}(z_k) = \delta_{jk}$. Como $\alpha_j(z), \beta_j(z) \in \PP_{2n+1}$, pueden expresarse en términos de $L_{n,j}(z)$ como

$$\alpha_j(z) = (c_0z+c_1)L^2_{n,j}(z), \quad \beta_j(z) = (c_2z+c_3)L^2_{n,j}(z), \quad j\in I_n. $$

De $\alpha_j^{(k)}(z_j)$ y $\beta_j^{(k)}(z_j), \qquad k=0,1$, se tiene

$$
\left\{
\begin{array}{ll}
c_0 = -2L'_{n,j}(z_j)\\
c_1 = 1 + 2z_jL'_{n,j}(z_j)\\
c_2 = 1\\
c_3 = -z_j
\end{array}
\right.
$$
\end{frame}

\begin{frame}
En efecto,

$$
\left\{
\begin{array}{ll}
\alpha_j(z) = [1 - 2(z-z_j)L'_{n,j}(z_j)]L_{n,j}^2(z)\\
\beta_j(z) = (z-z_j)L_{n,j}^2(z)
\end{array}
\right.
$$

De modo que se ha construido el conjunto siguiente
$H = \{\alpha_j(z), \beta_j(z) \, | \, j \in I_n\} \subset \PP_{2n+1}$. ?`Es $H$ es una base de $\PP_{2n+1}$?
\end{frame}

\begin{frame}
\begin{lemma}\label{hermitelemma}
Si $\{(z_j, w_j), (z_j,w'_j)\}_{j=0}^{n}$ un conjunto de $2n+2$ pares de puntos y $\dsty H = \bigcup_{j=0}^{n}\{\alpha_j(z), \beta_j(z)\}$, entonces $H$ es una base de $\PP_{2n+1}$.
\end{lemma}

\begin{proof}
Idea: Tomamos un polinomio $P\in \PP_{2n+1}$ arbitrariamente, lo expresamos como combinación lineal los polinomios en $H$ y exigimos que satisfaga las condiciones (\ref{condhermite}). Surge un sistema de ecuaciones que posee solución única. Asi que $H$ es una base de $\PP_{2n+1}$.
\end{proof}
\end{frame}

\begin{frame}
El sistema que resulta en la demostración del lema (\ref{hermitelemma}) es
  \begin{equation}\label{matrix_hermite}
  \left(\begin{array}{ccccccc}
  1 & 0 & \cdots & 0 & 0\\
  0 & 1 & \cdots & 0 & 0\\
  \vdots & \vdots & \vdots & \vdots &\vdots \\
  0 & 0 & \cdots & 1 & 0\\
  0 & 0 & \cdots & 0 & 1
  \end{array}\right )
  \left(\begin{array}{c} c_0\\c_1\\ \vdots \\c_{2n} \\c_{2n+1} \end{array}\right)=
  \left(\begin{array}{c} w_0\\w'_0\\ \vdots \\w_n \\w'_n \end{array}\right)
  \end{equation}

cuya solución es
$$c_{2j} = w_j, \quad, c_{2j+1} = w'_j\quad i \in I_n.$$
\end{frame}

\begin{frame}
  Así que el polinomio (\ref{polyhermite}) queda expresado por

  \begin{equation}
  \Pi_{2n+1}(z)=\sum_{j=0}^n[w_j\alpha_j(z)+w'_j\beta_j(z)],
  \end{equation}

  donde

  $$
  \left\{
  \begin{array}{ll}
  \alpha_j(z) = [1 - 2(z-z_j)L'_{n,j}(z_j)]L_{n,j}^2(z)\\
  \beta_j(z) = (z-z_j)L_{n,j}^2(z)
  \end{array}
  \right.
  $$

Este es conocido como la {\em forma de Hermite} de interpolación polinómica.
\end{frame}

\begin{frame}
\frametitle{Forma de Abel-Goncharov}

Como motivación presentemos el problema siguiente:\\

Sean $\{(z_{j}, w^{(j)}_{j})\}_{j=0}^{n}$ un conjunto $n+1$ pares de puntos, donde $w^{(j)}_{j}$ es la imagen de la derivada $j$-esima de una función $n$-veces diferenciable en $z_j$.\\

De manera natural surge la siguiente pregunta: ?`existe una función $\Pi$ única tal que
$\Pi^{(j)}(z_j) = w^{(j)}_j$? Afortunadamente, la respuesta es afirmativa. \\

El lema siguiente recoge estas ideas.
\end{frame}

\begin{frame}
\begin{lemma}\label{abel_gonch}
Si $\{(z_{j}, w^{(j)}_{j})\}_{j=0}^{n}$ son $n+1$ pares de puntos, entonces existe un único $\Pi_n \in \PP_n$ tal que $\Pi^{(j)}_n(z_j) = w^{(j)}_{j}, \quad j \in I_n$.
\end{lemma}

\begin{proof}
Idea: Solo basta tomar un polinomio $\Pi_n \in \PP_n$ tal que satisfaga $\Pi^{(j)}_n(z_j) = w^{(j)}_j$. Luego surge un sistema de ecuaciones cuya solución es única. Asi queda probada la existencia y unicidad de $\Pi_n$.
\end{proof}
\end{frame}

\begin{frame}
El sistema de ecuaciones que resulta en la demostración del lema (\ref{abel_gonch}) es

\begin{equation}\label{matrix_abel}
\left(\begin{array}{ccccc}
1 & z_0 & z_0^2 & \cdots & z_0^n\\
0 & 1 & 2z_1 & \cdots & z_1^{n-1}\\
\vdots & \vdots & \vdots &  & \vdots \\
0 & 0 & 0 & \cdots & n! \end{array}\right )
\left(\begin{array}{c} c_0\\c_1\\ \vdots \\c_n \end{array}\right)=
\left(\begin{array}{c} w^{(0)}_0\\w^{(1)}_1\\ \vdots \\w^{(n)}_n \end{array}\right)
\end{equation}

Tenemos la garantía de que tales polinomios existen y son únicos. Aunque, como se aprecia en la prueba, los $c_k$ resultan complicados de obtener en la base usual. Encontremos otra manera de representar $\Pi_n$.
\end{frame}

\begin{frame}
  Consideremos la ecuación diferencial\\[0.5cm]

  $\dsty \frac{d^{n}\Pi_n(z)}{dz^{n}} = w^{(n)}_n$ sujeta a ${\Pi_n^{(j)}=w^{(j)}_j \quad j=0,1,\ldots, n-1}$.

Como el lado derecho de la ecuación diferencial es constante, con lo cual es holomorfa sobre cualquier subconjunto $U$ de $\CC$.
\end{frame}

\begin{frame}
Integrando sucesivamente, tenemos

\begin{equation}\label{pol-abel-gon}
\Pi_n(z) = \sum_{k=0}^{n} \frac{w^{(k)}_k}{k!}G_{n,k}(z)
\end{equation}

donde

\begin{equation}
\left\{
\begin{array}{ll}
G_{n,0}(z) = 1\\
G_{n,k}(z) = \dsty k!\int_{z_{0}}^{z}\int_{z_{1}}^{\zeta_{k-1}} \cdots \int_{z_{k-1}}^{\zeta_{1}} d\zeta_{0}\cdots d\zeta_{k-1}, \quad k = 1, 2, \ldots, n.
\end{array}
\right.
\end{equation}

El polinomio de grado $\leq n$ dado en (\ref{pol-abel-gon}), se conoce como el {\em polinomio de Abel-Goncharov}.
\end{frame}

\begin{frame}
  En particular, si $f:U \subset \CC \longrightarrow \CC$ es $n$-veces diferenciable y si los nodos de interpolación son iguales. Esto es, ${z_0=z_1=\cdots=z_n} \in U$, entonces el polinomio que interpola $f^{(j)}(z_j)$ en $z_j$ viene dado por

  \begin{equation}\label{taylor}
  \Pi_n(z) = \sum_{k=0}^{n} \frac{f^{(k)}(z_0)}{k!} (z-z_0)^{k}
  \end{equation}

  La identidad (\ref{taylor}) se conoce como el {\em polinomio de interpolación de Taylor}.
\end{frame}
