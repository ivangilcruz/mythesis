\select@language {spanish}
\contentsline {chapter}{\numberline {1}Interpolaci\'{o}n Polinomial}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Introducci\'{o}n}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Teor\'{\i }a de interpolaci\'{o}n polin\'{o}mica}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Interpolaci\'{o}n de Lagrange}{5}{section.1.3}
\contentsline {section}{\numberline {1.4}Nodos de interpolaci\'{o}n uniformemente espaciados}{7}{section.1.4}
\contentsline {section}{\numberline {1.5}An\'{a}lisis del error de interpolaci\'{o}n por polinomios}{8}{section.1.5}
\contentsline {section}{\numberline {1.6}F\'{o}rmula de interpolaci\'{o}n de Newton}{12}{section.1.6}
\contentsline {section}{\numberline {1.7}Polinomios de Hermite}{17}{section.1.7}
\contentsline {chapter}{\numberline {2}Interpolaci\'{o}n con wxMaxima}{21}{chapter.2}
\contentsline {section}{\numberline {2.1}Introducci\'{o}n a wxMaxima}{21}{section.2.1}
\contentsline {chapter}{\numberline {3}Conclusiones y problemas propuestos}{23}{chapter.3}
\contentsline {section}{\numberline {3.1}Conclusiones}{23}{section.3.1}
\contentsline {section}{\numberline {3.2}Problemas propuestos}{23}{section.3.2}
\contentsline {chapter}{Bibliograf\'{\i }a}{25}{chapter*.2}
\contentsfinish 
